package com.mapx.kosten.mosimpa.di.modules.internments

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class InternmentsScope