package com.mapx.kosten.mosimpa.data.entities

data class InternmentsRsp(
    val id: String,
    val internments : List<Internment>
)