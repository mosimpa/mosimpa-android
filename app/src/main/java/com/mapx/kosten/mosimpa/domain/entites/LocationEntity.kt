package com.mapx.kosten.mosimpa.domain.entites

data class LocationEntity (
    val type: String = "",
    val desc: String = ""
)