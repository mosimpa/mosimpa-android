package com.mapx.kosten.mosimpa.domain.entites

data class PatientEntity (
    var name: String = "",
    var surname: String = "",
    var age: Int = -1 ,
    var gender: String = ""
)