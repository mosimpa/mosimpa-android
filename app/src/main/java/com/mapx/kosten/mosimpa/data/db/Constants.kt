package com.mapx.kosten.mosimpa.data.db

class Constants {

    companion object {
        const val SERVERS_TABLE = "serversTbl"
        const val INTERNMENTS_TABLE = "internmentsTbl"
        const val SENSOR_O2_TABLE = "sensorO2Tbl"
        const val SENSOR_BLOOD_TABLE = "sensorBloodTbl"
        const val SENSOR_HEART_TABLE = "sensorHeartTbl"
        const val SENSOR_TEMP_TABLE = "sensorTempTbl"
    }
}