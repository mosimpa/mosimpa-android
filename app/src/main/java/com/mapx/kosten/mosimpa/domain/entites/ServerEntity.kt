package com.mapx.kosten.mosimpa.domain.entites

data class ServerEntity (
    var id: Long = -1,
    var name: String = "",
    var ip: String = "",
    var port: Int = -1
)