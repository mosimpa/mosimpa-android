# MoSimPa

MoSimPa Android App  was designed with clean architecture implemented the S.O.L.I.D. principles.

## Layers
- Data
- Domain
- Presentation

## Libraries

- RxJava2
- LiveData
- Navigation Component
- Room
- Dagger
- Corutines
- MPAndroidChart
- Paho MQTT Client


